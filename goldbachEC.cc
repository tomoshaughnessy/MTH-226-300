#include <iostream>

int main()
{
	bool sieve[1000000];
	for (int i = 0; i < 1000001; i++){
		sieve[i] = 1;
	}
	sieve[0] = 0;						// neither 0
	sieve[1] = 0;						// nor 1 are prime

	for (int i = 2; i < 1000; i++){				// starting at 2, find all multiples of each number leading up to the square root of amount of indices in sieve
		if (sieve[i] == 1)				// if it hasn't been marked yet, it could be a factor of a number in the sieve, so work with it
			for(int j = i*i; j < 1000000; j+=i)	// start with the square of it, then mark every multiple 
				sieve[j] = 0;			// if composite, set it to FALSE
	}
	
	int primes[78497];					// number of primes below 1000000 = 78498
	int p = 0;
	for (int i = 0; i < 1000001; i++){			
		if (sieve[i] == 1){
			primes[p] = i;
			p++;
		}
	}							// since 1000000 is a large number
	for (int goldbachNumber = 1000000; goldbachNumber < 1000202; goldbachNumber+=2){
		for (int i = 78497; i > -1; i--){		// starting with the largest prime makes the algorithm faster (it's closer to 1000000/2)
			int goldbachPartition = goldbachNumber - primes[i];
			if (sieve[goldbachPartition] == 1){
				std::cout << goldbachNumber << " = " << goldbachPartition << " + " << primes[i] << "\t\t";
				break;
			}
		}
	}
	std::cout << "\n";
	return 0;
}

// ALGORITHM:
// compute sieve of Erastosthenes
// create list of primes from sieve
// LOOP
//	 start at arbitrary Goldbach number: Gn
//	 subtract from it prime[i]: Pi
//	 check for a Goldbach partition: Gp
//	 Gn - Pi = Gp (may not be Gp)
//	 if sieve[Gp] is 1 (TRUE or prime)
//		then we may output:
//		Gn = Pi + Gp
//	 	Gn+=2
//	 else
//	 	Pi++
