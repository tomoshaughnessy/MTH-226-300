#include <iostream>

int stealFish(int fish, int sleepingFishermen);

int main()
{
	int barrel;
	int fishcaught;
	for (int i = 1; ; i+=3){
		barrel = stealFish(i, 3);
		if (barrel >= 0){		// non-negative value
			fishcaught = i;		
			break;			// solution was found
		}
	}
	std::cout << "The answer is: " << fishcaught << "\n";
	return 0;
}

int stealFish(int fish, int sleepingFishermen)
{
	if (sleepingFishermen == 0)			// no fisherman left
		return fish;
	sleepingFishermen--;				// fisherman wakes up
	if (fish % 3 == 1)				// extra fish in barrel
		fish--;					// he throws it away
	else
		return -1;				// incorrect value, return error
	fish -= (fish/3);				// fisherman takes 1/3 of fish
	fish = stealFish(fish, sleepingFishermen);	// recurse until all fisherman have "taken their share"
	return fish;
}
